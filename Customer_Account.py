class CustomerAccount:
    def __init__(self, customer_id, customer_name, account_balance):
        self.customer_id=customer_id
        self.customer_name=customer_name
        self.account_balance=account_balance
        
    def deposit(self, amount):
        self.account_balance += amount
        trans_log=TransactionLog()
        trans_log.log_transaction(f"Deposit amount : {amount} ")
        
    def withdraw(self, amount):
        self.account_balance -= amount 
        trans_log=TransactionLog()
        trans_log.log_transaction(f"Withdrawal amount: {amount} ")
class TransactionLog:
    def log_transaction(self,transaction_details):
        print(transaction_details)

customer1=CustomerAccount("001874229", "Poghos Poghosyan", 70000)
print(f"{customer1.customer_name}'s account balance: ",customer1.account_balance)
customer1.deposit(14700)
print(f"{customer1.customer_name}'s account balance: ",customer1.account_balance)
customer1.withdraw(5000)
print(f"{customer1.customer_name}'s account balance: ",customer1.account_balance)
